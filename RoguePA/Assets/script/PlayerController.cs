using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	private int jumpHeight = 15;
	private float moveSpeed = 5;


	void Start ()
	{


	} 
	void Update ()
	{
		if (Input.GetKeyDown(KeyCode.W)) 
		{
			GetComponent<Rigidbody2D>().velocity = new Vector3(0, jumpHeight, 0);
		}

		if (Input.GetKey(KeyCode.D))
		{
			GetComponent<Rigidbody2D>().velocity = new Vector3(moveSpeed, GetComponent<Rigidbody2D>().velocity.y, 0);		                                   
		}

		if (Input.GetKey(KeyCode.A)) 
		{
			GetComponent<Rigidbody2D>().velocity = new Vector3(moveSpeed, GetComponent<Rigidbody2D>().velocity.y, 0);
		}
	} }