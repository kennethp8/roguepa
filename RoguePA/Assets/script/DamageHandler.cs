﻿using UnityEngine;
using System.Collections;

public class DamageHandler : MonoBehaviour
{
    public int health = 1;

    public float invulnPeriod = 0;
    float invulnTimer = 0;
    int correctLayer;

    SpriteRenderer Damage;

    void Start()
    {
        correctLayer = gameObject.layer;
        Damage = GetComponent<SpriteRenderer>();
        if (Damage == null)
        {
            Damage = transform.GetComponentInChildren<SpriteRenderer>();
            if (Damage == null)
            {
                Debug.LogError("Object'" + gameObject.name + "' has no sprite renderer");
            }
        }
    }

    void OnTriggerEnter2D()
    {
        health--;
        invulnTimer = 1.05f;
        if (invulnPeriod > 0)
        {
            invulnTimer = invulnPeriod;
            gameObject.layer = 10;
        }
    }
    void Update()
    {


        if (invulnTimer <= 0)
        {
            invulnTimer -= Time.deltaTime;

            if (invulnTimer <= 0)
            {
                gameObject.layer = correctLayer;
                if (Damage != null)
                {
                    Damage.enabled = true;
                }
            }
            else
            {
                if (Damage != null)
                {
                    Damage.enabled = !Damage.enabled;
                }
            }
        }

        if (health <= 0)
        {
            Die();
        }
    }
    void Die()
    {
        Destroy(gameObject);
    }

}
